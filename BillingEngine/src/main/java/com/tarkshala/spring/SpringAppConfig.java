package com.tarkshala.spring;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Spring application configuration, It is one of the place where beans can be initialized manually.
 * Other place is spring-config.xml, which is loaded as resource into this config object.
 */
@Configuration
@ImportResource("classpath:spring-config.xml")
public class SpringAppConfig {

    @Bean
    public AmazonDynamoDBClientBuilder getAmazonDynamoDBClientBuilder() {
        return AmazonDynamoDBClientBuilder.standard().withRegion(Regions.AP_SOUTH_1);
    }

    @Bean
    public DynamoDBMapper getDynamoDBMapper(final AmazonDynamoDBClientBuilder builder) {
        return new DynamoDBMapper(builder.build());
    }

    @Bean
    public DynamoDB getDynamoDB(final AmazonDynamoDBClientBuilder builder) {
        return new DynamoDB(builder.build());
    }

    @Bean
    public Gson getGson() {
        return new GsonBuilder().create();
    }
}
