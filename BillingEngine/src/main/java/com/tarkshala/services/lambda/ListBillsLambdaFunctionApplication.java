package com.tarkshala.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.services.lambda.model.ListBillsRequest;
import com.tarkshala.services.lambda.model.ListBillsResponse;
import com.tarkshala.spring.SpringApplication;

public class ListBillsLambdaFunctionApplication extends SpringApplication
        implements RequestHandler<ListBillsRequest, ListBillsResponse> {

    @Override
    public ListBillsResponse handleRequest(ListBillsRequest request, Context context) {
        return getBean(ListBillsLambdaFunction.class).handleRequest(request, context);
    }
}
