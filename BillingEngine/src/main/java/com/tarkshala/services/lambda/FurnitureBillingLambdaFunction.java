package com.tarkshala.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.dao.BillDAO;
import com.tarkshala.dao.model.BillDO;
import com.tarkshala.dao.model.BillItemDO;
import com.tarkshala.services.lambda.model.BillItemRO;
import com.tarkshala.services.lambda.model.FurnitureBillingRequest;
import com.tarkshala.services.lambda.model.FurnitureBillingResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class FurnitureBillingLambdaFunction implements RequestHandler<FurnitureBillingRequest, FurnitureBillingResponse> {

    @Autowired
    private BillDAO billDAO;

    public FurnitureBillingResponse handleRequest(FurnitureBillingRequest furnitureBillingRequest, Context context) {

        BillDO billDO = convertIntoBillDO(furnitureBillingRequest);
        billDAO.create(billDO);
        return new FurnitureBillingResponse("Bill created with id [" + billDO.getUid() + "]");
    }

    private BillDO convertIntoBillDO(FurnitureBillingRequest billingRequest) {

        String uuid = UUID.randomUUID().toString();
        List<BillItemDO> billItems = billingRequest.getItems().stream().map(converter).collect(Collectors.toList());
        BillDO billDO = new BillDO(uuid,
                billingRequest.getName(),
                billingRequest.getDate(),
                billingRequest.getPhone(),
                billItems,
                billingRequest.getTotalAmount(),
                billingRequest.getAmountPaid(),
                billingRequest.getRemark(),
                billingRequest.getAddress());

        return billDO;
    }

    private static Function<BillItemRO, BillItemDO> converter = billItemRO -> {

        BillItemDO billItemDO = new BillItemDO();
        billItemDO.setName(billItemRO.getName());
        billItemDO.setPrice(billItemRO.getPrice());
        billItemDO.setRemark(billItemRO.getRemark());
        billItemDO.setWarranty(billItemRO.getWarranty());

        return billItemDO;
    };
}
