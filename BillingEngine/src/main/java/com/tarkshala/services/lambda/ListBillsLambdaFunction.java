package com.tarkshala.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.dao.BillDAO;
import com.tarkshala.dao.model.BillDO;
import com.tarkshala.dao.model.ListBillsQO;
import com.tarkshala.services.lambda.model.BillBO;
import com.tarkshala.services.lambda.model.ListBillsRequest;
import com.tarkshala.services.lambda.model.ListBillsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class ListBillsLambdaFunction implements RequestHandler<ListBillsRequest, ListBillsResponse> {

    @Autowired
    private BillDAO billDAO;

    @Override
    public ListBillsResponse handleRequest(ListBillsRequest listBillsRequest, Context context) {

        ListBillsQO billsQO = ListBillsQO.builder()
                .phone(listBillsRequest.getPhone())
                .name(listBillsRequest.getName())
                .startDate(listBillsRequest.getStartTime())
                .endDate(listBillsRequest.getEndTime())
                .pageStart(listBillsRequest.getPageStart())
                .pageEnd(listBillsRequest.getPageEnd()).build();
        List<BillDO> billDOs = billDAO.read(billsQO);
        List<BillBO> bills = billDOs.stream().map(convertDOtoBO).collect(Collectors.toList());
        return ListBillsResponse.builder().bills(bills).build();
    }

    private Function<BillDO, BillBO> convertDOtoBO = billDO -> BillBO.builder()
            .uid(billDO.getUid())
            .name(billDO.getName())
            .date(billDO.getDate())
            .phone(billDO.getPhone())
            .address(billDO.getAddress())
            .totalAmount(billDO.getTotalAmount())
            .amountPaid(billDO.getAmountPaid())
            .remark(billDO.getRemark()).build();

}
