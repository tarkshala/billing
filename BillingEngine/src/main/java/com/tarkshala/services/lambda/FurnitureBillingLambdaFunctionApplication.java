package com.tarkshala.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.services.lambda.model.FurnitureBillingRequest;
import com.tarkshala.services.lambda.model.FurnitureBillingResponse;
import com.tarkshala.spring.SpringApplication;

/**
 * Abstraction of FurnitureBillingLambdaFunction.
 * All of the logic is made to be present in {@link FurnitureBillingLambdaFunction}.
 * This helps to organise main logic better with spring DI with
 * annotations like {@link org.springframework.beans.factory.annotation.Autowired}.
 */
public class FurnitureBillingLambdaFunctionApplication extends SpringApplication
        implements RequestHandler<FurnitureBillingRequest, FurnitureBillingResponse> {

    public FurnitureBillingResponse handleRequest(FurnitureBillingRequest request, Context context) {
        return getBean(FurnitureBillingLambdaFunction.class).handleRequest(request, context);
    }
}
