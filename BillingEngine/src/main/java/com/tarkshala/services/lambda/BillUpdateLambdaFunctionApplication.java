package com.tarkshala.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.services.lambda.model.UpdateBillRequest;
import com.tarkshala.services.lambda.model.UpdateBillResponse;
import com.tarkshala.spring.SpringApplication;

public class BillUpdateLambdaFunctionApplication extends SpringApplication
        implements RequestHandler<UpdateBillRequest, UpdateBillResponse> {

    @Override
    public UpdateBillResponse handleRequest(UpdateBillRequest updateBillRequest, Context context) {
        return getBean(BillUpdateLambdaFunction.class).handleRequest(updateBillRequest, context);
    }
}
