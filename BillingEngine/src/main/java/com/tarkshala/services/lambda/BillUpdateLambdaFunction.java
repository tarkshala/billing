package com.tarkshala.services.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.tarkshala.dao.BillDAO;
import com.tarkshala.dao.model.BillDO;
import com.tarkshala.dao.model.BillItemDO;
import com.tarkshala.services.lambda.model.BillItemRO;
import com.tarkshala.services.lambda.model.UpdateBillRequest;
import com.tarkshala.services.lambda.model.UpdateBillResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class BillUpdateLambdaFunction implements RequestHandler<UpdateBillRequest, UpdateBillResponse> {

    @Autowired
    private BillDAO billDAO;

    @Override
    public UpdateBillResponse handleRequest(UpdateBillRequest updateBillRequest, Context context) {
        BillDO billDO = convertIntoBillDO(updateBillRequest);
        billDAO.update(billDO);
        return UpdateBillResponse.builder().message("Bill updated for [" + billDO.getUid() + "]").build();
    }

    private BillDO convertIntoBillDO(UpdateBillRequest updateBillRequest) {

        List<BillItemDO> billItems = updateBillRequest.getItems().stream().map(converter).collect(Collectors.toList());
        return new BillDO(updateBillRequest.getUid(),
                updateBillRequest.getName(),
                updateBillRequest.getDate(),
                updateBillRequest.getPhone(),
                billItems,
                updateBillRequest.getTotalAmount(),
                updateBillRequest.getAmountPaid(),
                updateBillRequest.getRemark(),
                updateBillRequest.getAddress());

    }

    private static Function<BillItemRO, BillItemDO> converter = billItemRO -> {

        BillItemDO billItemDO = new BillItemDO();
        billItemDO.setName(billItemRO.getName());
        billItemDO.setPrice(billItemRO.getPrice());
        billItemDO.setRemark(billItemRO.getRemark());
        billItemDO.setWarranty(billItemRO.getWarranty());

        return billItemDO;
    };

}
