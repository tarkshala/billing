package com.tarkshala.dao.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@DynamoDBDocument
public class BillItemDO {

    /**
     * Name of the item
     */
    private String name;

    /**
     * Price of the item
     */
    private int price;

    /**
     * Date till it hold warranty
     */
    private long warranty;

    /**
     * Additional remark
     */
    private String remark;
}
