package com.tarkshala.dao.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;


// TODO: 13/05/18 Use table name from a property file
@DynamoDBTable(tableName = "Furniture-Bills")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BillDO {

    @DynamoDBHashKey(attributeName = "uid")
    private String uid;

    /**
     * Name of the customer/person whom this bill refers
     */
    @DynamoDBAttribute(attributeName = "name")
    private String name;

    /**
     * Date of the billing
     */
    @DynamoDBIndexRangeKey(globalSecondaryIndexName = "phone-date-index")
    @DynamoDBAttribute(attributeName = "date")
    private long date;

    /**
     * Phone number of the person
     */
    @DynamoDBIndexHashKey(globalSecondaryIndexName = "phone-date-index")
    @DynamoDBAttribute(attributeName = "phone")
    private String phone;

    /**
     * List of items included in the bill
     */
    @DynamoDBAttribute(attributeName = "items")
    private List<BillItemDO> items;

    /**
     * Total amount might not be sum of prices of all the items.
     * It is gross amount that is demanded after discount.
     */
    @DynamoDBAttribute(attributeName = "totalAmount")
    private Integer totalAmount;

    /**
     * Amount paid is the actual amount that is paid by customer against
     * the bill.
     *
     * e.g. While billing the <code>totalAmount</code> was 1000/- rupees
     * and the customer paid only 700/- then rest 300/- will be his debt.
     */
    @DynamoDBAttribute(attributeName = "amountPaid")
    private Integer amountPaid;

    /**
     * Bill wide remark
     */
    @DynamoDBAttribute(attributeName = "remark")
    private String remark;

    /**
     * Address of the customer
     */
    @DynamoDBAttribute(attributeName = "address")
    private String address;

    /**
     * Add a new item to the bill.
     *
     * @param item to be added
     */
    public void addItem(BillItemDO item) {
        if (items == null) items = new ArrayList<>();
        items.add(item);
    }
}
