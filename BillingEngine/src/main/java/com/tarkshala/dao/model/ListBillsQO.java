package com.tarkshala.dao.model;

import lombok.Builder;
import lombok.Getter;

/**
 * List bills query object
 */
@Builder
@Getter
public class ListBillsQO {

    private String phone;

    private String name;

    private long startDate;

    private long endDate;

    private int pageStart;

    private int pageEnd;
}
