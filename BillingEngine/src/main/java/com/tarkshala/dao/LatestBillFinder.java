package com.tarkshala.dao;

import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.internal.IteratorSupport;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.google.gson.Gson;
import com.tarkshala.dao.model.BillDO;
import com.tarkshala.dao.model.ListBillsQO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class LatestBillFinder implements BillFinder {

    // TODO: 13/05/18 Use it from a property file
    private static String TABLE_NAME = "Furniture-Bills";

    @Autowired
    private DynamoDB dynamoDB;

    @Autowired
    private Gson gson;

    @Override
    public List<BillDO> find(ListBillsQO billsQO) {

        Table table = dynamoDB.getTable(TABLE_NAME);
        ScanSpec spec = createScanSpec(billsQO);
        ItemCollection<ScanOutcome> items = table.scan(spec);
        IteratorSupport<Item, ScanOutcome> iterator = items.iterator();
        List<BillDO> bills = new ArrayList<>();
        while (iterator.hasNext()) {
            Item item = iterator.next();
            bills.add(gson.fromJson(item.toJSON(), BillDO.class));
        }
        return bills;
    }

    private ScanSpec createScanSpec(ListBillsQO billsQO) {
        return new ScanSpec().withFilterExpression("#date between :start_date and :end_date")
                .withNameMap(new NameMap().with("#date", "date"))
                .withValueMap(new ValueMap().withNumber(":start_date", billsQO.getStartDate()).
                        withNumber(":end_date", billsQO.getEndDate()));
    }
}
