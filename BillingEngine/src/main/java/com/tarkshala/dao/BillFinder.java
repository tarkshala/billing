package com.tarkshala.dao;

import com.tarkshala.dao.model.BillDO;
import com.tarkshala.dao.model.ListBillsQO;

import java.util.List;

public interface BillFinder {

    List<BillDO> find(ListBillsQO billsQO);
}
