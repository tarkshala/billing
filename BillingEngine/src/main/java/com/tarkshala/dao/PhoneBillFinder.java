package com.tarkshala.dao;

import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.google.gson.Gson;
import com.tarkshala.dao.model.BillDO;
import com.tarkshala.dao.model.ListBillsQO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class PhoneBillFinder implements BillFinder {

    // TODO: 13/05/18 Use it from a property file
    private static String TABLE_NAME = "Furniture-Bills";
    private static String  INDEX_NAME = "phone-date-index";
    private static String INDEX_KEY = "phone";

    @Autowired
    private DynamoDB dynamoDB;

    @Autowired
    private Gson gson;

    @Override
    public List<BillDO> find(ListBillsQO billsQO) {

        Table table = dynamoDB.getTable(TABLE_NAME);
        Index index = table.getIndex(INDEX_NAME);

        QuerySpec spec = createQuerySpec(billsQO);
        ItemCollection<QueryOutcome> items = index.query(spec);

        Iterator<Item> iterator = items.iterator();
        List<BillDO> bills = new ArrayList<>();
        while (iterator.hasNext()) {
            Item item = iterator.next();
            bills.add(gson.fromJson(item.toJSON(), BillDO.class));
        }

        return bills;
    }

    private QuerySpec createQuerySpec(ListBillsQO billsQO) {
        return new QuerySpec().withHashKey(INDEX_KEY, billsQO.getPhone());
    }
}
