package com.tarkshala.dao;

import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.internal.IteratorSupport;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.google.gson.Gson;
import com.tarkshala.dao.model.BillDO;
import com.tarkshala.dao.model.ListBillsQO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class NameBillFinder implements BillFinder{

    private static String TABLE_NAME = "Furniture-Bills";
    private static String  INDEX_NAME = "phone-date-index";
    private static String INDEX_KEY = "phone";

    @Autowired
    private DynamoDB dynamoDB;

    @Autowired
    private Gson gson;

    @Override
    public List<BillDO> find(ListBillsQO billsQO) {

        Table table = dynamoDB.getTable(TABLE_NAME);

        Map<String, Object> expressionAttributeValues = new HashMap<>();
        expressionAttributeValues.put(":v_name", billsQO.getName());
        Map<String, String> attributeNamesMap = new HashMap<>();
        attributeNamesMap.put("#name", "name");
        ScanSpec spec = new ScanSpec().withFilterExpression("contains(#name, :v_name)")
                .withNameMap(attributeNamesMap)
                .withValueMap(expressionAttributeValues);

        List<BillDO> bills = new ArrayList<>();

        ItemCollection<ScanOutcome> items = table.scan(spec);
        IteratorSupport<Item, ScanOutcome> iterator = items.iterator();
        while (iterator.hasNext()) {
            Item item = iterator.next();
            bills.add(gson.fromJson(item.toJSON(), BillDO.class));
        }

        return bills;
    }
}
