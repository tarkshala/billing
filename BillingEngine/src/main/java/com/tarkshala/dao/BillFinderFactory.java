package com.tarkshala.dao;

import com.tarkshala.dao.model.ListBillsQO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BillFinderFactory {

    @Autowired
    private NameBillFinder nameBillFinder;

    @Autowired
    private PhoneBillFinder phoneBillFinder;

    @Autowired
    private LatestBillFinder latestBillFinder;

    public BillFinder getInstance(ListBillsQO listBillsQO) {

        if (StringUtils.isNotEmpty(listBillsQO.getPhone())) {
            return phoneBillFinder;
        } else if (StringUtils.isNotEmpty(listBillsQO.getName())) {
            return nameBillFinder;
        }
        return latestBillFinder;
    }

}
