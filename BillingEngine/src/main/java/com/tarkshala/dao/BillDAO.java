package com.tarkshala.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.tarkshala.dao.model.BillDO;
import com.tarkshala.dao.model.ListBillsQO;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Billing DAO.
 * It takes care of all the required CRUD operations on bills.
 *
 * @author kuldeep
 */
@Component
@AllArgsConstructor()
public class BillDAO {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Autowired
    private BillFinderFactory billFinderFactory;

    /**
     * Create a bill.
     *
     * @param billDO to be saved into system.
     */
    public void create(BillDO billDO) {
        dynamoDBMapper.save(billDO);
    }

    /**
     * Read bills as per fields specified in query object
     *
     * @param billsQO the bill query object
     * @return list of bills qualifying for QO
     */
    public List<BillDO> read(ListBillsQO billsQO) {
        return billFinderFactory.getInstance(billsQO).find(billsQO);
    }

    /**
     * Update a bill
     * @param billDO to be updated.
     */
    public void update(BillDO billDO) {
        dynamoDBMapper.save(billDO);
    }
}
