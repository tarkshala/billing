package com.tarkshala;

import com.tarkshala.services.lambda.model.BillItemRO;
import com.tarkshala.services.lambda.model.FurnitureBillingRequest;

import java.util.Collections;
import java.util.Date;

public class AbstractTest {

    protected FurnitureBillingRequest getSampleBillingRequest() {

        FurnitureBillingRequest request = new FurnitureBillingRequest();
        request.setAmountPaid(4000);
        request.setDate(new Date().getTime());
        request.setItems(Collections.singletonList(getSampleBillItemRO("Cooler1")));
        request.setPhone("9535580366");
        request.setName("Kuldeep");
        request.setTotalAmount(5000);

        return request;
    }

    protected BillItemRO getSampleBillItemRO() {
        return getSampleBillItemRO("Cooler");
    }

    protected BillItemRO getSampleBillItemRO(String name) {
        BillItemRO billItemRO = new BillItemRO();
        billItemRO.setName(name);
        billItemRO.setRemark("New body old motor");
        billItemRO.setPrice(5000);
        billItemRO.setWarranty(new Date().getTime());

        return billItemRO;
    }

}
