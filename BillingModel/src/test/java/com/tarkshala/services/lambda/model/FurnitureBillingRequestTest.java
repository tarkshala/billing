package com.tarkshala.services.lambda.model;

import com.tarkshala.AbstractTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.*;

public class FurnitureBillingRequestTest extends AbstractTest {

    private FurnitureBillingRequest request;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void toJson() {
        FurnitureBillingRequest billRequest = getSampleBillingRequest();
        System.out.println(billRequest.toJson());
        FurnitureBillingRequest deserializeRequest = new FurnitureBillingRequest().toObject(billRequest.toJson());

        Assert.assertEquals(billRequest.getName(), deserializeRequest.getName());
        Assert.assertEquals(billRequest.getTotalAmount(), deserializeRequest.getTotalAmount());
        Assert.assertEquals(billRequest.getPhone(), deserializeRequest.getPhone());
        Assert.assertEquals(billRequest.getAmountPaid(), deserializeRequest.getAmountPaid());
        Assert.assertEquals(billRequest.getDate(), deserializeRequest.getDate());
        Assert.assertEquals(billRequest.getItems().get(0).getName(), deserializeRequest.getItems().get(0).getName());
        Assert.assertEquals(billRequest.getItems().get(0).getPrice(), deserializeRequest.getItems().get(0).getPrice());
        Assert.assertTrue(billRequest.getItems().contains(deserializeRequest.getItems().get(0)));
    }

    @Test
    public void toObject() {
        toJson();
    }
}