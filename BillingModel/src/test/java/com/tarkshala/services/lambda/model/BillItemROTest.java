package com.tarkshala.services.lambda.model;

import com.tarkshala.AbstractTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BillItemROTest extends AbstractTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void toJson() {
        BillItemRO item = getSampleBillItemRO();
        String json = item.toJson();
        Assert.assertTrue(json.contains(item.getRemark()));
    }

    @Test
    public void toObject() {
        String itemName = "pump";
        BillItemRO item = getSampleBillItemRO(itemName);
        String json = item.toJson();
        BillItemRO deserializedItemRO = new BillItemRO().toObject(json);
        Assert.assertEquals(item, deserializedItemRO);
    }
}