package com.tarkshala.services.lambda.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class UpdateBillRequestTest {

    @Test
    public void toJson() {

        UpdateBillRequest request = UpdateBillRequest.builder()
                .date(1234567868L)
                .uid("9i09909-90i90-8u90")
                .name("kuldeep")
                .phone("9535580366")
                .build();

        System.out.println(request.toJson());
    }

    @Test
    public void toObject() {
    }
}