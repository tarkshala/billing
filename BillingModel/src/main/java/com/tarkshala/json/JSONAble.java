package com.tarkshala.json;

public interface JSONAble<T> extends java.io.Serializable {

    String toJson();

    // FIXME: 06/05/18 Check if it can be made a static method
    T toObject(String json);
}
