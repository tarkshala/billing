package com.tarkshala.services.lambda.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.tarkshala.json.JSONAble;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateBillRequest implements JSONAble<UpdateBillRequest> {

    private static final Gson gsonSerializer;

    static {
        gsonSerializer = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    @Expose
    private String uid;

    @Expose
    protected String name;

    @Expose
    protected long date;

    @Expose
    protected String phone;

    @Expose
    protected List<BillItemRO> items;

    @Expose
    protected int totalAmount;

    @Expose
    protected int amountPaid;

    @Expose
    protected String remark;

    @Expose
    protected String address;

    public String toJson() {
        return gsonSerializer.toJson(this);
    }

    public UpdateBillRequest toObject(String json) {
        return gsonSerializer.fromJson(json, UpdateBillRequest.class);
    }
}
