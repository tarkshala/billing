package com.tarkshala.services.lambda.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.tarkshala.json.JSONAble;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ListBillsRequest implements JSONAble<ListBillsRequest> {

    private static final Gson gsonSerializer;

    static {
        gsonSerializer = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    // Page range to decide number of bills to be picked
    @Expose
    private int pageStart;

    @Expose
    private int pageEnd;

    // Filters to be applied on search
    @Expose
    private long startTime;

    @Expose
    private long endTime;

    @Expose
    private String phone;

    @Expose
    private String name;

    @Expose
    private String address;

    @Override
    public String toJson() {
        return gsonSerializer.toJson(this);
    }

    @Override
    public ListBillsRequest toObject(String json) {
        return gsonSerializer.fromJson(json, ListBillsRequest.class);
    }
}
