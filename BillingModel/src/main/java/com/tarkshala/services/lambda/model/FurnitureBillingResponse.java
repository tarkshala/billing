package com.tarkshala.services.lambda.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.tarkshala.json.JSONAble;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FurnitureBillingResponse implements JSONAble<FurnitureBillingResponse> {

    private static final Gson gsonSerializer;

    static {
        gsonSerializer = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    @Expose
    private String message;

    @Override
    public String toJson() {
        return gsonSerializer.toJson(this);
    }

    @Override
    public FurnitureBillingResponse toObject(String json) {
        return gsonSerializer.fromJson(json, FurnitureBillingResponse.class);
    }
}
