package com.tarkshala.services.lambda.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.tarkshala.json.JSONAble;
import lombok.*;

import java.util.List;

@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class ListBillsResponse implements JSONAble<ListBillsResponse> {

    private static final Gson gsonSerializer;

    static {
        gsonSerializer = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    /**
     * List of bills
     */
    @Expose
    private List<BillBO> bills;

    @Override
    public String toJson() {
        return gsonSerializer.toJson(this);
    }

    @Override
    public ListBillsResponse toObject(String json) {
        return gsonSerializer.fromJson(json, ListBillsResponse.class);
    }
}
