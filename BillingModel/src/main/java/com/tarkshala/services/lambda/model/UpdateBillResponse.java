package com.tarkshala.services.lambda.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.tarkshala.json.JSONAble;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UpdateBillResponse implements JSONAble<UpdateBillResponse> {

    private static final Gson gsonSerializer;

    static {
        gsonSerializer = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    @Expose
    private String message;

    @Override
    public String toJson() {
        return gsonSerializer.toJson(this);
    }

    @Override
    public UpdateBillResponse toObject(String json) {
        return gsonSerializer.fromJson(json, UpdateBillResponse.class);
    }
}
