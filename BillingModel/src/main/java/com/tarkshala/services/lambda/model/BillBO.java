package com.tarkshala.services.lambda.model;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.tarkshala.json.JSONAble;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BillBO implements JSONAble<BillBO> {

    private static final Gson gsonSerializer;

    static {
        gsonSerializer = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    @Expose
    private String uid;

    @Expose
    private String name;

    @Expose
    private long date;

    @Expose
    private String phone;

    @Expose
    private List<BillItemRO> items;

    @Expose
    private int totalAmount;

    @Expose
    private int amountPaid;

    @Expose
    private String remark;

    @Expose
    private String address;

    @Override
    public String toJson() {
        return gsonSerializer.toJson(this);
    }

    @Override
    public BillBO toObject(String json) {
        return gsonSerializer.fromJson(json, BillBO.class);
    }
}
