package com.tarkshala.services.lambda.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.tarkshala.json.JSONAble;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

/**
 * Bill item request object
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BillItemRO implements JSONAble<BillItemRO> {

    private static final Gson gsonSerializer;

    static {
        gsonSerializer = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    /**
     * Name of the item
     */
    @Expose
    private String name;

    /**
     * Price of the item
     */
    @Expose
    private int price;

    /**
     * Date till it hold warranty
     */
    @Expose
    private long warranty;

    /**
     * Additional remark
     */
    @Expose
    private String remark;

    @Override
    public String toJson() {
        return gsonSerializer.toJson(this);
    }

    @Override
    public BillItemRO toObject(String json) {
        return gsonSerializer.fromJson(json, BillItemRO.class);
    }

    @Override
    public boolean equals(Object that) {

        if (!(that instanceof BillItemRO)) {
            return false;
        }
        BillItemRO thatBillItemRO = (BillItemRO) that;

        if (!StringUtils.equals(this.name, thatBillItemRO.name)) return false;

        if (price != thatBillItemRO.getPrice()) return false;

        if (warranty != thatBillItemRO.getWarranty()) return false;

        if (!StringUtils.equals(this.remark, thatBillItemRO.remark)) return false;

        return true;
    }
}
