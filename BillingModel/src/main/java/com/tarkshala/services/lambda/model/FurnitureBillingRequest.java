package com.tarkshala.services.lambda.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.tarkshala.json.JSONAble;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FurnitureBillingRequest implements JSONAble<FurnitureBillingRequest> {

    private static final Gson gsonSerializer;

    static {
        gsonSerializer = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    @Expose
    protected String name;

    @Expose
    protected long date;

    @Expose
    protected String phone;

    @Expose
    protected List<BillItemRO> items;

    @Expose
    protected int totalAmount;

    @Expose
    protected int amountPaid;

    @Expose
    protected String remark;

    @Expose
    protected String address;

    public FurnitureBillingRequest(String name, long date, String phone, int totalAmount,
                                   int amountPaid, String remark, String address) {
        this(name, date, phone, new ArrayList<>(), totalAmount, amountPaid, remark, address);
    }

    public String toJson() {
        return gsonSerializer.toJson(this);
    }

    public FurnitureBillingRequest toObject(String json) {
        return gsonSerializer.fromJson(json, FurnitureBillingRequest.class);
    }

    @Override
    public boolean equals(Object that) {

        if (!(that instanceof FurnitureBillingRequest)) {
            return false;
        }

        FurnitureBillingRequest thatFurnitureBillingRequest = (FurnitureBillingRequest) that;

        if (!StringUtils.equals(this.name, thatFurnitureBillingRequest.name)) return false;

        if (this.date != thatFurnitureBillingRequest.date) return false;

        if (!StringUtils.equals(this.phone, thatFurnitureBillingRequest.phone)) return false;

        if (!this.items.equals(thatFurnitureBillingRequest.items)) return false;

        if (this.totalAmount != thatFurnitureBillingRequest.totalAmount) return false;

        if (this.amountPaid != thatFurnitureBillingRequest.amountPaid) return false;

        return true;
    }
}
